package com.google.zxing.client.android.Utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;

import com.google.zxing.WriterException;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.google.zxing.client.android.encode.QRCodeEncoder;


/**
 * Created by guich on 2016/12/7.
 */

public class ZXingUtils {

    //调用扫码活动，通过requestCode返回
    public static void startCaptureForResult(Activity context, int requestCode) {
        Intent intent = new Intent(context, CaptureActivity.class);
        context.startActivityForResult(intent, requestCode);
    }

    //通过string返回二维码图片
    public static Bitmap getBitmapByString(String text) {
        Intent intent = new Intent();
        intent.putExtra(Intents.Encode.DATA, text);
        QRCodeEncoder qrCodeEncoder;
        Bitmap bitmap = null;
        try {
            qrCodeEncoder = new QRCodeEncoder(intent, 800);
            bitmap = qrCodeEncoder.encodeAsBitmap();
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
