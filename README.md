### 基于ZXing3.3.0简化的安卓二维码识别与生成类库

myapplication为一个基本调用的demo
myZxing既是核心类库，基于Zxing3.3.0简化

添加到自己的项目中gradle
```
dependencies {
    compile project(':myZXing')
}
```
打开扫码界面：
```
ZXingUtils.startCaptureForResult((Activity) getContext(), 0);
```
获取扫码结果：
```
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            String scanResult = data.getStringExtra("result_string");
            Log.d("scanResult:", scanResult);
            etCreate.setText(scanResult);
        }
    }
```

根据字符串产生二维码
```
qrCode.setImageBitmap(ZXingUtils.getBitmapByString(text));
```


